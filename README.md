# CP Internal Training

## Aufgaben

### Teil 1: HTML, CSS und JS Basics

Setze eine Startseite in HTML5 und CSS3 (SASS) unter Berücksichtigung von Mobile First um. Halte dich dabei an die Vorlage
in Zeplin für die Mobil und die Desktop Variante. Optimiere die Tablet Variante nach deinen eigenen Vorstellungen.

Design:
https://app.zeplin.io/project/5df102c14987b9afd88589f8/screen/5df102fdfa45ecad8e9eed1c

#### Acceptance Criteria

1. HTML ist semantisch  korrekt und modular strukturiert
3. Es wurde normalize.css verwendet
2. CSS in SASS mit Variablen für wiederkehrende Werte (z.B. Farben), Nesting, Mixins und Inheritance
3. Sass Mixin implementiert für Responsive Design
4. Wenn möglich auf floating in CSS verzichten und Flexbox und/oder Grids nutzen
5. Die Navi, die auf Desktop beim Punkt "Library" erscheint soll ohne JavaScript umgesetzt werden
6. Auch die "Zitate" sollen ohne JavaScript umgesetzt werden
7. Die Navi auf Mobile soll mit Hilfe eines selbstgewählten jQuery Plugins funktionieren 
[jpanelmenu](http://jpanelmenu.com/index.html)
8. Der Header auf Mobile soll, mit Hilfe eines JS Plugins (nicht jQuery), sticky sein, wenn man nach oben scrollt und verschwinden wenn man nach unten scrollt 
[Headroom.js](https://wicky.nillia.ms/headroom.js/)

#### Ressources

[HTML Structuring Basics](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure)
[Normalize CSS](https://necolas.github.io/normalize.css/)
[SASS Guide](https://sass-lang.com/guide)
[SASS Responsive Mixin](https://medium.com/developing-with-sass/creating-a-dead-simple-sass-mixin-to-handle-responsive-breakpoints-889927b37740)
[CSS Flexbox](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox)
[CSS Grids](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids)

#### TL;DR

[What is Webpack](https://medium.com/the-self-taught-programmer/what-is-webpack-and-why-should-i-care-part-1-introduction-ca4da7d0d8dc)

### Teil 2: jQuery für Beginner

Coming soon...

### Teil 3: Und jetzt ohne jQuery

Coming soon...

### Teil 4: EcmaScript6 ist dein Freund

Coming soon...

### Teil 5: Hallo PHP

Coming soon...

### Teil 5: Pflegbare Inhalte objektorientiert

Coming soon...

### Teil 5: MVC ist geil

Coming soon...

## Setup

### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.
